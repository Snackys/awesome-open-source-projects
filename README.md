# Awesome Open Source Projects



## Project management tools


[AppFlowy](https://www.appflowy.io/) => Notion

[Taiga](https://www.taiga.io/) => Trello


## Design tools


[Penpot](https://penpot.app/) => Figma

[Iconduck](https://iconduck.com/)


## Music maker tools


[LMMS](https://lmms.io/) => FL Studio

[Audacity](https://www.audacityteam.org/)


## Video game maker tools

[Godot](https://godotengine.org/) => Unity
